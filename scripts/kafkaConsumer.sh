#!/usr/bin/env bash

MASTER="local[4]"
CONFIGPATH="."
PROGRAM="target/scala-2.12/RiskIdent.jar"
MAIN=SparkKafkaConsumer
OUT=out.out
ERR=err.err
if [[ $DEBUG ]];then
    export SPARK_SUBMIT_OPTS=-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5005
fi

spark-submit \
--packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.0 \
--class $MAIN \
--master $MASTER \
--conf "spark.driver.extraClassPath=$CONFIGPATH" \
$PROGRAM "$@" 2>$ERR |tee $OUT

echo Output is in $OUT, error output in $ERR
