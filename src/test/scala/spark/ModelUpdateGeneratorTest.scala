package spark

import common.ModelParametersValues.givenModelParameters
import common.{DataMsg, Point}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import testutils.SparkSessionTestWrapper

class ModelUpdateGeneratorTest extends AnyFlatSpec with Matchers with SparkSessionTestWrapper {

	implicit val sparkSession = spark

	import spark.implicits._

	"ModelUpdateGenerator" should
		"generate a model update every 100 points, repeated for every partition" in {

		val points = Seq(
			UnifiedData(0, null, DataMsg(1, Point(0, 0, 0, 0, 0), 0.0)),
			UnifiedData(1, null, DataMsg(100, Point(0, 0, 0, 0, 0), 0.0)),
			UnifiedData(2, null, DataMsg(120, Point(0, 0, 0, 0, 0), 0.0)),
			UnifiedData(3, null, DataMsg(200, Point(0, 0, 0, 0, 0), 0.0))
		)

		val partitions = 2

		val expectedUpdates = Seq(
			UnifiedData(0, givenModelParameters(1), null),
			UnifiedData(1, givenModelParameters(1), null),
			UnifiedData(0, givenModelParameters(2), null),
			UnifiedData(1, givenModelParameters(2), null)
		)

		val modelUpdates = ModelUpdateGenerator.get(spark.createDataset(points), partitions).collect()
		modelUpdates.foreach(println)
		modelUpdates should be(expectedUpdates)

	}

}
