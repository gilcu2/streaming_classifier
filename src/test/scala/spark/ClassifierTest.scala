package spark

import common.ModelParametersValues.givenModelParameters
import common.{DataMsg, Point}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import testutils.SparkSessionTestWrapper

class ClassifierTest extends AnyFlatSpec with Matchers with SparkSessionTestWrapper {

	implicit val sparkSession = spark

	import spark.implicits._

	"Classifier" should
		"classify each point with the last model arrived" in {

		val partitions = 2

		val data = Seq(
			UnifiedData(0, givenModelParameters(1), null),
			UnifiedData(1, givenModelParameters(1), null),
			UnifiedData(0, null, DataMsg(1, Point(0, 0, 0, 0, 0), 0.0)),
			UnifiedData(1, null, DataMsg(100, Point(0, 0, 0, 0, 0), 0.0)),
			UnifiedData(0, givenModelParameters(2), null),
			UnifiedData(1, givenModelParameters(2), null),
			UnifiedData(0, null, DataMsg(120, Point(0, 0, 0, 0, 0), 0.0)),
			UnifiedData(1, null, DataMsg(200, Point(0, 0, 0, 0, 0), 0.0))
		)

		val classification = Classifier.classify(spark.createDataset(data).repartition(partitions)).collect()

		classification.foreach(println)
		classification.map(_.results.modelUsed) should be(Seq(1, 1, 2, 2))

	}
}
