package common

object ModelParametersValues {

	val givenModelParameters = Array(
		UpdateModelMsg(0, ModelParameters(6.08, 7.78, 6.34, 8.05, 3.14, 61.35)),
		UpdateModelMsg(1, ModelParameters(8.46, 1.74, 6.08, 4.25, 1.92, 71.37)),
		UpdateModelMsg(2, ModelParameters(6.53, 5.46, 0.0, 9.95, 6.29, 43.3)),
		UpdateModelMsg(3, ModelParameters(3.2, 7.32, 1.46, 2.29, 4.26, 94.81)),
		UpdateModelMsg(4, ModelParameters(2.71, 0.82, 8.54, 0.21, 2.1, 66.25))
	)

}
