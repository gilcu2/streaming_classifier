package common

case class Point(x1: Double, x2: Double, x3: Double, x4: Double, x5: Double)

case class DataMsg(id: Long, input: Point, label: Double)

case class UpdateModelMsg(modelId: Int, parameters: ModelParameters)

case class ClassificationResult(id: Long, classLabel: Double, modelUsed: Int)

object Data {

	val r = scala.util.Random

	def create(id: Long) = {
		val input = Point(r.nextDouble, r.nextDouble, r.nextDouble, r.nextDouble, r.nextDouble)
		val label = if (r.nextDouble() > 0.5) 1 else 0
		DataMsg(id, input, label)
	}

}