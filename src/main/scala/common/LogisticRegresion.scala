package common

import java.lang.Math._

case class ModelParameters(w1: Double, w2: Double, w3: Double, w4: Double, w5: Double, b: Double)

class LogisticRegression(val p: ModelParameters) {

	def classify(in: Point): Double = {

		val linearPart = p.b + p.w1 * in.x1 + p.w2 * in.x2 + p.w3 * in.x3 + p.w4 * in.x4 + p.w5 * in.x5
		val expPart = exp(linearPart)
		val probability = expPart / (1 + expPart)
		if (probability > 0.5) 1.0 else 0.0
	}

}
