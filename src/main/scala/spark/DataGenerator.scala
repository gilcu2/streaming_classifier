package spark

import common.UpdateModelMsg
import common.{Data, DataMsg}
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

object DataGenerator {

	def get(rate: Int, partitions: Int)(implicit spark: SparkSession): Dataset[DataMsg] = {
		import spark.implicits._
		val rateStream = createRateStream(rate, spark)
		mapToDataPoints(rateStream)
	}

	def mapToDataPoints(df: DataFrame)(implicit spark: SparkSession): Dataset[DataMsg] = {
		import spark.implicits._
		df.map(r => {
			val id = r.getAs[Long]("value")
			Data.create(id)
		})
	}

	private def createRateStream(rate: Int, spark: SparkSession) = {
		spark
			.readStream
			.format("rate") // <-- use RateStreamSource
			.option("rowsPerSecond", rate)
			.load
	}
}
